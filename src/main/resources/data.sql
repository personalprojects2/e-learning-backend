INSERT INTO STUDENT(id,name,user_name,password,date_of_birth,email,gender) values(1,'stud1','studname1','123','2019-12-12','stud1@gmail.com','male');
INSERT INTO STUDENT(id,name,user_name,password,date_of_birth,email,gender) values(2,'stud2','studname2','123','2019-12-12','stud2@gmail.com','male');
INSERT INTO STUDENT(id,name,user_name,password,date_of_birth,email,gender) values(3,'stud3','studname3','123','2019-12-12','stud3@gmail.com','male');
INSERT INTO STUDENT(id,name,user_name,password,date_of_birth,email,gender) values(4,'stud4','studname4','123','2019-12-12','stud4@gmail.com','male');
INSERT INTO STUDENT(id,name,user_name,password,date_of_birth,email,gender) values(5,'stud5','studname5','123','2019-12-12','stud5@gmail.com','male');
INSERT INTO STUDENT(id,name,user_name,password,date_of_birth,email,gender) values(6,'stud6','studname6','123','2019-12-12','stud6@gmail.com','male');

INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(1,'course1','coursedescription1','instructor1',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(2,'course2','coursedescription2','instructor2',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(3,'course3','coursedescription3','instructor3',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(4,'course4','coursedescription4','instructor4',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(5,'course5','coursedescription5','instructor5',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(6,'course6','coursedescription6','instructor6',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(7,'course7','coursedescription7','instructor7',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(8,'course8','coursedescription8','instructor8',12,'2019-12-12','2019-12-12');
INSERT INTO COURSE(id,name,description,instructor,total_hours,last_updated,publish_date) values(9,'course9','coursedescription9','instructor9',12,'2019-12-12','2019-12-12');