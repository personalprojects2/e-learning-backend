package com.Three2one.e_learning.endpoint.rest;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.dto.StudentDTO;
import com.Three2one.e_learning.business.exceptions.BadRequestException;
import com.Three2one.e_learning.business.service.StudentService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(maxAge = 2 ^ 63 - 1)
@RestController
@RequestMapping("/student")
public class StudentRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentRestController.class);

	@Autowired
	private StudentService studentService;

	@ApiOperation(value = "Get Student List", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Student List retreived successfully", response = StudentDTO.class, responseContainer = "List"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<StudentDTO> getAllStudents() {
		LOGGER.info("Receiving a Request for getting list of all students.");
		return studentService.getAllStudentsDTOS();
	}

	@ApiOperation(value = "Get Courses registered by Student.", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Student Course List retreived successfully", response = CourseDTO.class, responseContainer = "List"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@GetMapping(value = "/courses/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CourseDTO> getStudentRegisteredCourse(@PathVariable("id") Long studentId) {
		LOGGER.info("Receiving a Request for getting list of all Courses registered by student with Id {} ", studentId);
		return studentService.getRegisteredCoursesForStudent(studentId);
	}

	@ApiOperation(value = "Get Student Data with Id.", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Student retreived successfully", response = StudentDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public StudentDTO getStudentById(@PathVariable("id") Long id) {
		LOGGER.info("Receiving a Request for getting a Student with id : {} ", id);
		StudentDTO studentDTO = studentService.getStudentDTOById(id);
		if (studentDTO == null) {
			LOGGER.error("Student with Id {} doesn't exists.", id);
			throw new BadRequestException("Student with Id {" + id + "} doesn't exists.");
		}
		return studentDTO;
	}

	@ApiOperation(value = "delete Student Data with Id.", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Student deleted successfully", response = StudentDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteStudent(@PathVariable("id") Long id) {
		LOGGER.info("Receiving a Request for getting a Student with id : {} ", id);
		StudentDTO studentDTO = studentService.getStudentDTOById(id);
		if (studentDTO == null) {
			LOGGER.error("Student with Id {} doesn't exists.", id);
			throw new BadRequestException("Student with Id {" + id + "} doesn't exists.");
		}
		studentService.deleteStudentDTOById(id);
	}

	@ApiOperation(value = "Save Student Data.", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Student saved successfully", response = StudentDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@PutMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public StudentDTO saveStudent(@Valid @RequestBody StudentDTO studentDTO) {
		LOGGER.info("Receiving a Request for Saving a Student");
		if (studentDTO == null) {
			LOGGER.error("StudentDto must be provided.");
			throw new BadRequestException("StudentDto must be provided.");
		}
		return studentService.saveStudentDTO(studentDTO);
	}

	@ApiOperation(value = "update Student Data.", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Student saved successfully", response = StudentDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@PutMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public StudentDTO updateStudent(@PathVariable long id, @Valid @RequestBody StudentDTO studentDTO) {
		LOGGER.info("Receiving a Request for Saving a Student");
		StudentDTO returnedStudentDTO = studentService.getStudentDTOById(id);
		if (returnedStudentDTO == null) {
			LOGGER.error("StudentDto must be provided.");
			throw new BadRequestException("StudentDto must be provided.");
		}
		return studentService.saveStudentDTO(studentDTO);
	}

	@ApiOperation(value = "Register a Student to a Course.", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Student registered successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@PutMapping(value = "/register")
	public void registerCourse(@RequestParam(value = "course_id", required = true) Long courseId,
			@RequestParam(value = "student_id", required = true) Long studentId) {
		LOGGER.info("Receiving a Request for Registering a Student with Id : {} to Course Id : {} ", studentId,
				courseId);
		studentService.registerStudentToCourse(studentId, courseId);
	}

	@ApiOperation(value = "UnRegister a Student to a Course.", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Student unregistered successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@PutMapping(value = "/unregister")
	public void unregisterCourse(@RequestParam(value = "course_id", required = true) Long courseId,
			@RequestParam(value = "student_id", required = true) Long studentId) {
		LOGGER.info("Receiving a Request for UnRegistering a Student with Id : {} to Course Id : {} ", studentId,
				courseId);
		studentService.unRegisterStudentToCourse(studentId, courseId);
	}

}
