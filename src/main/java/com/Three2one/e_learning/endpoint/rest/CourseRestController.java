package com.Three2one.e_learning.endpoint.rest;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.exceptions.BadRequestException;
import com.Three2one.e_learning.business.service.CourseService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(maxAge = 30000)
@RestController
@RequestMapping("/course")
public class CourseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CourseRestController.class);

	@Autowired
	private CourseService courseService;

	@ApiOperation(value = "Get Course List", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Course List retreived successfully", response = CourseDTO.class, responseContainer = "List"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CourseDTO> getAllCourses() {
		LOGGER.info("Receiving a Request for getting List of Courses.");
		return courseService.getAllCoursesDTOS();
	}

	@ApiOperation(value = "Get Course Data with Id", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Course retreived successfully", response = CourseDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public CourseDTO getCourse(@PathVariable("id") Long id) {
		LOGGER.info("Receiving a Request for getting a Course with id : {} ", id);
		CourseDTO courseDTO = courseService.getCourseDTOById(id);
		if (courseDTO == null) {
			LOGGER.error("Course with Id {} doesn't exists.", id);
			throw new BadRequestException("Course with Id {" + id + "} doesn't exists.");
		}
		return courseDTO;
	}

	@ApiOperation(value = "delete Course Data with Id", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Course deleted successfully", response = CourseDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteCourse(@PathVariable("id") Long id) {
		LOGGER.info("Receiving a Request for getting a Course with id : {} ", id);
		CourseDTO courseDTO = courseService.getCourseDTOById(id);
		if (courseDTO == null) {
			LOGGER.error("Course with Id {} doesn't exists.", id);
			throw new BadRequestException("Course with Id {" + id + "} doesn't exists.");
		}
		courseService.deleteCourseDTOById(id);
	}

	@ApiOperation(value = "Save Course Data.", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Course saved successfully", response = CourseDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@PutMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public CourseDTO saveCourse(@Valid @RequestBody CourseDTO courseDTO) {
		CourseDTO savedCourseDTO = courseService.saveCourseDTO(courseDTO);
		return savedCourseDTO;
	}

	@ApiOperation(value = "update Course Data.", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Course saved successfully", response = CourseDTO.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), })
	@PutMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public CourseDTO updateCourse(@PathVariable long id, @Valid @RequestBody CourseDTO courseDTO) {
		LOGGER.info("Receiving a Request for getting a Course with id : {} ", id);
		CourseDTO returnedCourseDTO = courseService.getCourseDTOById(id);
		if (returnedCourseDTO == null) {
			LOGGER.error("Course with Id {} doesn't exists.", id);
			throw new BadRequestException("Course with Id {" + id + "} doesn't exists.");
		}
		CourseDTO savedCourseDTO = courseService.saveCourseDTO(courseDTO);
		return savedCourseDTO;
	}
}
