package com.Three2one.e_learning.business.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.entity.Course;

@Component
public class CourseMapper {

	public List<CourseDTO> getCourseDTOsFromEntities(List<Course> courseEntities) {
		List<CourseDTO> courseDTOs = new ArrayList<>();
		// If Passed List is empty
		if (courseEntities == null || courseEntities.isEmpty()) {
			return courseDTOs;
		}
		for (Course courseEntity : courseEntities) {
			CourseDTO courseDTO = getCourseDTOFromEntity(courseEntity);
			courseDTOs.add(courseDTO);
		}
		// return DTOS
		return courseDTOs;
	}

	public CourseDTO getCourseDTOFromEntity(Course courseEntity) {
		if (courseEntity == null) {
			return null;
		}
		CourseDTO courseDTO = new CourseDTO();
		courseDTO.setId(courseEntity.getId());
		courseDTO.setName(courseEntity.getName());
		courseDTO.setDescription(courseEntity.getDescription());
		courseDTO.setInstructor(courseEntity.getInstructor());
		courseDTO.setLastUpdated(courseEntity.getLastUpdated());
		courseDTO.setPublishDate(courseEntity.getPublishDate());
		courseDTO.setTotalHours(courseEntity.getTotalHours());
		// return CourseDto
		return courseDTO;
	}

	public Course getCourseEntityFromDTO(CourseDTO courseDTO) {
		if (courseDTO == null) {
			return null;
		}
		Course courseEntity = new Course();
		courseEntity.setId(courseDTO.getId());
		courseEntity.setName(courseDTO.getName());
		courseEntity.setDescription(courseDTO.getDescription());
		courseEntity.setInstructor(courseDTO.getInstructor());
		courseEntity.setLastUpdated(courseDTO.getLastUpdated());
		courseEntity.setPublishDate(courseDTO.getPublishDate());
		courseEntity.setTotalHours(courseDTO.getTotalHours());
		// return courseEntity
		return courseEntity;
	}

}
