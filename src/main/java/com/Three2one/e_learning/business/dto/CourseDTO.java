package com.Three2one.e_learning.business.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Course Details Model.")
public class CourseDTO {

	@ApiModelProperty(notes = "Course Id (auto generated)", example = "1", required = true, position = 0)
	private long id;

	@NotBlank(message = "Course Name is mandatory")
	@ApiModelProperty(notes = "Course Name", example = "Java Course", required = true, position = 1)
	private String name;

	@ApiModelProperty(notes = "Course Description", example = "Java Course for beginners", required = false, position = 2)
	private String description;

	@ApiModelProperty(notes = "Publish Date", example = "yyyy-mm-dd", required = true, position = 3)
	private Date publishDate;

	@ApiModelProperty(notes = "Last Updated date", example = "yyyy-mm-dd", required = false, position = 4)
	private Date lastUpdated;

	@ApiModelProperty(notes = "Total Hours", example = "40", required = true, position = 5)
	private Integer totalHours;

	@NotBlank(message = "Course Instructor is mandatory")
	@ApiModelProperty(notes = "Instructor Name", example = "Mark", required = true, position = 6)
	private String instructor;

}
