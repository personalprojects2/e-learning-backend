package com.Three2one.e_learning.business.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Student Details Model.")
public class StudentDTO {

	@ApiModelProperty(notes = "Student Id (auto generated)", example = "1", required = true, position = 0)
	private long id;

	@NotBlank(message = "Student Name is mandatory")
	@ApiModelProperty(notes = "Student Name", example = "Mahmoud Matar", required = true, position = 1)
	private String name;

	@NotBlank(message = "Student Email is Mandatory")
	@ApiModelProperty(notes = "Student Email", example = "mahmoudMatar@gmail.com", required = true, position = 2)
	private String email;

	@NotBlank(message = "Student UserName is mandatory")
	@ApiModelProperty(notes = "Username", example = "mahmoudgamal", required = true, position = 3)
	private String userName;

	@ApiModelProperty(notes = "Date Of Birth", example = "yyyy-MM-dd", required = true, position = 4)
	private Date dateOfBirth;

	@ApiModelProperty(notes = "Gender", example = "Male", required = true, position = 5)
	private String gender;

	@NotBlank(message = "Student Password is mandatory")
//	@JsonIgnore
	@ApiModelProperty(notes = "password", example = "mypassword", required = true, position = 6)
//	@ToString.Exclude
	private String password;

}
