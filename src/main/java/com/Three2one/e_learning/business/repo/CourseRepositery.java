package com.Three2one.e_learning.business.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.Three2one.e_learning.business.entity.Course;

public interface CourseRepositery extends CrudRepository<Course, Long> {

	List<Course> findByName(String name);

}
